学んだこと
機能： モデル、ビュー、コントローラーの関係性を理解出来た。

Git： エラーが出てしまい、以前のコミットへ戻る方法や、事前にブランチを切っておくことの重要さを理解できた。

苦労したこと
求められている実装が期限を過ぎても終わらなかったこと。

イマイチな点
いいね機能、検索機能、コメント機について実装が出来なかった。

デザイン： レイアウトまで気が回らず、Railsチュートリアルライクなサイトとなってしまった。


中途半端なサイトとなりレビューしていただくのも恐縮ですが、期限をすぎておりますので提出させていただきます。